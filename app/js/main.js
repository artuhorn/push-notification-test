'use strict';

var API_KEY_SERVER = 'AIzaSyCjUkgnjOKyoqqevIzjkTj-M1hh2rSQ-YU';
var API_KEY = 'AIzaSyBE9FbjymdRN1BchGcKRhROzqW6k7JDZkI';
var registrationId = 'cSusGhGnXj0:APA91bEZEgW-mwVn5lDLwonQG4hWXMd66dcz2o8Ekkiizz23a1u0P1zieIozNLi36frPAsPhIgQxOXn_OnZemJ_JEx9dgebm-mXFFv1CsHULywoX_N9Jyxaz7Zg-ygBNf8QXJR37gF6r';


if ('serviceWorker' in navigator) {
  console.log('Service Worker is supported');

  navigator.serviceWorker.register('sw.js')
    .then(function(reg) {
      console.log(':^)', reg);
      reg.pushManager.subscribe({
        userVisibleOnly: true
      }).then(function(sub) {
        console.log('endpoint:', sub.endpoint);
        console.log(sub.endpoint.split('/'));
      });
    }).catch(function(error) {
    console.log(':^(', error);
  });
}


document.addEventListener('DOMContentLoaded', function() {

  var button = document.querySelector('.js-send');
  button.addEventListener('click', function() {

    //fetch('https://android.googleapis.com/gcm/send',
     fetch("https://gcm-http.googleapis.com/gcm/send",

      {
        headers: new Headers({
          'Authorization': 'key=' + API_KEY,
          'Content-Type': 'application/json'
        }),
        method: "post",
        body: JSON.stringify({
          //registration_ids: [registrationId]
          'to': registrationId,
          "collapse_key": "price_update",
          "notification": {
            "title": "Portugal vs. Denmark",
            "text": "5 to 1"
          },
          "data": {
            "custom_value":"1555"
          }
        })

      })
      .then(function(res) {
        var result = res;
        console.log(result);

        var arrayBuffer = res.body; // Note: not oReq.responseText
        var str = '';
        if (arrayBuffer) {
          console.log('Buffer', arrayBuffer);
          var byteArray = new Uint8Array(arrayBuffer);
          console.log('Length', byteArray.byteLength);
          for (var i = 0; i < byteArray.byteLength; i++) {
            str += byteArray[i];
          }
        }

        console.log(str);

        return result;
      })
      .catch(function(error) {
        console.log(error);
      })
  });

}, false);